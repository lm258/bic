#!/bin/bash

#First get the directories
for dir in `ls output/`
do
    #Remove old mine file if exists
    rm -f output/${dir}/mine.txt

    #now get input files in each directory
    for res in `ls output/${dir}/run_*.txt`
    do
        tail -n 1 $res | awk '{print $3 " " $6 " " $10 " " $13 " " $17 " " $20}' >> output/${dir}/mine.txt
    done

    #Now run over and generate our averages
    echo ${dir}
    #g+sizepop,trainpc, trainmse, testpc, testmse, bsftest
    awk '{ pop += $1; trainpc += $2; trainmse += $3; testpc += $4; testmse += $5; bsftest += $6; } END { print "Avg Pop: " pop/NR ", Avg Train MSE:" trainmse/NR "(" trainpc/NR "%) Avg Test MSE:" testmse/NR "(" testpc/NR "%),  best: " bsftest/NR }' output/${dir}/mine.txt
done