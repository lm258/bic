#!/bin/bash

#How many times run each experiment
export RUN_TIMES=20

#Loop over each experiment
for i in `ls inputs/experiments/*`
do
    #First get experiment name
    exp_name=${i##*/}
    exp_name=${exp_name%.*}

    #Type of the experiment ie box, extended box etc
    exp_type=${i##*.}

    #Next create folder in output
    mkdir -p output/$exp_name
    mkdir -p output/$exp_name/inputs

    #Loop over for each experiment
    for j in `seq 1 $RUN_TIMES`
    do
        #Set the outfile for the experiment
        OUTFILE=output/$exp_name/outfile_$j.txt

        #Set the output for the command
        STD_OUTFILE=output/$exp_name/run_$j.txt

        #Set the name of our input file
        INPUTFILE=output/$exp_name/inputs/run_$j.txt

        #Next generate our new input file
        eval "echo \"$(< $i)\"" > $INPUTFILE

        #Finally execute the command
        echo "Running - "$exp_name" - run - "$j
        ./bin/ann_$exp_type $INPUTFILE > $STD_OUTFILE
    done
done