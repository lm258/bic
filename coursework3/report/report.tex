%
% Author - Lewis Maitland
% Date   - 13/10/2014
% Brief  - This is the main page for my
%          dissertation report. Includes
%          the other pages for tidyness.

\documentclass[a4paper,11pt]{report}

\usepackage[dvipsnames,usenames]{color,xcolor,colortbl}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{array}
\usepackage{listings}
\usepackage{color}
\usepackage{lewis}

\lstdefinestyle{param_file}{
  basicstyle=\tiny,        % the size of the fonts that are used for the code
  captionpos=b,                    % sets the caption-position to bottom
  extendedchars=true,
  keepspaces=true,
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  tabsize=2,                       % sets default tab size to 2 spaces
}

\begin{document}

%Make the title page of our document
\title{\textbf{ECJ Experimentation Report}}
\author{Lewis Maitland\\\footnotesize lm258@hw.ac.uk\\\footnotesize H00016037 \\\tiny Rev:\input{revision.hash}}
\maketitle

%Start on the first page about parameter choices
\section*{Parameter Choices}

\subsection*{Population Size}
Population size was increased during my analysis of its effects on GP. I decided to increase the population rather than decrease it, to find out whether a larger population is beneficial to finding a solution. The different problems had different starting populations ranging from 100-1024. During my experiments the population for each benchmark problem was increased to 10000. I think having a larger population may be beneficial because it would allow more initial variety, meaning the GP might be less likely to head towards a local maxima and branch out down a variety of paths.

\subsection*{Mutation}
Mutation rate and the severity of its effects where tested. ECJ offered various different mechanisms for changing mutation in species. The full genome or segments of it could have different mutation probabilities and types. ECJs different mutation types included bit flipping, uniform and gaussian mutation, and also random walk mutation. My experiments involved having a high probability of random walk mutation, thereby \textit{walking} along the genome changing values randomly. I decided to use this mutation type as i wanted to monitor the effects of mutation with a high probability inducing large changes on genomes of the population. The probability of mutation was 100\% with the probability of continuing along the same genome at 90\%. I think that mutation within a population is beneficial because it may help solutions break free from local maxima, however too much mutation might produce unstable populations. I am checking to see if over mutation in GP is detrimental to its performance.

\subsection*{Crossover}
Crossover parameter changes where used to find out what effect different crossover functions had on the overall performance of GP. ECJ allows for several different crossover configurations. Crossover in GP uses the Koda node selection class. During my investigation i decided to test the effects of node selection but with a much higher preference to terminals rather than nodes. I think this will adversely effect the performance of GP because it will limit the evolve-ability of solutions. Terminals will have a 90\% chance of selection during cross over and non-terminals will have a 10\% chance.

\subsection*{Selection}
Selection in ECJ can have various different configurations. These configurations include tournament, best selection, first in list, random selection, sigma scaling and also stochastic sampling. I decided to test best selection first, this would take select a smaller population from the overall population and take the fittest individuals from it. I was going to strictly select only thee best individual from the whole population but this could lead to the GP getting stuck on local maxima. My best selection samples 100 individuals and then takes 1 individual from the 100.

\subsection*{Function Set}
Each problem used different function sets of varying sizes. Although i could have rearranged the sets or added additional functions to check performance i decided to reduce the number of functions available. ECJ offers ADF functionality and also the ability to specify grammars. However i think that by reducing the number of functions the GP can use, its overall complexity would be reduced. I was testing to see if reducing the complexity would reduce its ability to solve complex problems. Essentially i reduced each problems function set to size 3, checking to see the effect it had on overall performance.

% This page is used to describe the problems i chose for GP
\newpage
\section*{Problems}

\subsection*{Santa Fe Trail Ant}
The Santa Fe problem is used to benchmark the performance of GP algorithms. Essentially it consists of a 2D grid populated by a trail of ant food. The ant has to then move around the grid eating as much food as possible. The more food the ant eats the better the GP algorithm has performed. I think this problem is useful for GP benchmarking my tests because it involves programming the behavior of the ant so that it will perform optimally in its environment. This means that changing ECJs parameters will create observable changes in the ants behavior and thus its performance.\\
\begin{center}
    \includegraphics[width=1.6in]{img/santafe.jpg}
\end{center}

\subsection*{Klandscapes}
K-Landscapes are a type of problem that can also be used to test the effectiveness of a GP algorithm. K-Landscapes model a multi-dimensional landscape, the landscape can vary in roughness meaning several false peaks may be present. I decided to use K-Landscapes as i thought it would be a good problem to check that my parameter changes such as over mutation may be beneficial in moving solutions from local maxima to overall good solutions.
\begin{center}
    \includegraphics[width=1.6in]{img/kfitness.png}
\end{center}

\subsection*{Lawnmower}
John Kozas lawnmower problem involves genetically programming a lawnmower to cut a field of grass. The field is a 2D grid that the lawnmower can navigate around. The more grass that the lawnmower cuts the better the GP algorithm has performed. Similar to the Santa Fe problem the lawnmower problem requires the agent to perform optimally in its environment. GP is used to program the lawn mower, using a tree containing functions from its function set.
\begin{center}
    \includegraphics[width=1.6in]{img/lawn_mower.png}
\end{center}

% Here is a summary of my experiments
\newpage
\section*{Experiments}
My experiments consisted of changing the five different ECJ parameters for population size, mutation, crossover, selection and function set size when applied to three different problems. Each experiment was run 5 times to get a rough average for the performance of each experiment. Different random seeds where used during each run of the experiments. \textbf{Vanilla} experiments where the parameters are kept unchanged are used to benchmark the changes against those with changed parameters.\\

\textbf{Crossover Params}
\lstinputlisting{../ecj/ec/app/parent_params/crossover.params}~\\

\textbf{Function Set Size Params}
\lstinputlisting{../ecj/ec/app/parent_params/fun_size.params}~\\

\textbf{Mutation Params}
\lstinputlisting{../ecj/ec/app/parent_params/mutation.params}~\\

\textbf{Population Params}
\lstinputlisting{../ecj/ec/app/parent_params/pop.params}~\\

\textbf{Selection Params}
\lstinputlisting{../ecj/ec/app/parent_params/sel.params}~\\


% Start our results here
\newpage
\section*{Results}
Each table contains the averages of 5 runs for each different parameter change. Default was the default parameter file with no changes.

\subsection*{Santa Fe Trail Ant}
\renewcommand\arraystretch{0.6} \setlength\minrowclearance{0.6pt} 
\begin{tabular}{ | m{0.9in}| m{1.1in}| m{0.9in} | m{1.2in} | m{1.4in} | }
  %Create the title
  \dtitle{Param}{Total Memory}{Total Time}{Generations}{Best Fitness}

  %Enter the data
  \drow{Default}{155502KB}{1.052s}{50}{3.63032\%}
  \drow{Crossover}{129562KB}{1.1s}{50}{2.17243\%}
  \drow{Function Set}{80294.4KB}{0.586s}{50}{1.14943\%}
  \drow{Mutation}{156646KB}{1.018s}{50}{4.70562\%}
  \drow{Population}{593691KB}{7.096s}{35.6}{64.625\%}
  \drow{Selection}{147786KB}{1.02s}{50}{3.86192\%}
\end{tabular}


\subsection*{KLandscapes}
\begin{tabular}{ | m{0.9in}| m{1.1in}| m{0.9in} | m{1.2in} | m{1.4in} | }
  %Create the title
  \dtitle{Param}{Total Memory}{Total Time}{Generations}{Best Fitness}

  %Enter the data
  \drow{Default}{31108KB}{0.132s}{20.6}{100\%}
  \drow{Crossover}{35512.8KB}{0.16s}{48.4}{100\%}
  \drow{Function Set}{28930.4KB}{0.132s}{2.2}{100\%}
  \drow{Mutation}{35206.4KB}{0.158s}{59.6}{100\%}
  \drow{Population}{109608KB}{0.604s}{0.8}{100\%}
  \drow{Selection}{34100.8KB}{0.154s}{40.2}{100\%}
\end{tabular}


\subsection*{Lawnmower}
\begin{tabular}{ | m{0.9in}| m{1.1in}| m{0.9in} | m{1.2in} | m{1.4in} | }
  %Create the title
  \dtitle{Param}{Total Memory}{Total Time}{Generations}{Best Fitness}

  %Enter the data
  \drow{Default}{135784KB}{0.456s}{13.6}{100\%}
  \drow{Crossover}{96157.6KB}{0.448s}{50}{4.06923\%}
  \drow{Function Set}{79456.8KB}{0.404s}{50}{1.5625\%}
  \drow{Mutation}{122986KB}{0.434s}{13}{100\%}
  \drow{Population}{696953KB}{3.14s}{10.4}{100\%}
  \drow{Selection}{125273KB}{0.464s}{13.8}{100\%}
\end{tabular}

% Conclusion
\section*{Conclusion}
Based on my results i found that changing the various ECJ parameters affected the problems in varying ways. Reducing the function set size adversely effected both the Santa Fe Trail and lawnmower problems by increasing the total memory and reducing fitness. I think that total memory increased dramatically because of bloat, if the GP had limited functions available then other functions would need to be used repeatedly to emulate that behavior. For example if turn\_left was missing but turn\_right was there, turn\_left could be replaced by turn\_right, turn\_right, turn\_right. Interestingly KLandscapes performed better with the reduced function set than any other parameter change memory and time taken decreased. Increasing the population reduced the number of generations and also increased fitness where appropriate across all solutions. However the larger population required more memory and time when generating the solution across all problems. Mutation parameter changes had minor effects on memory and total time for the Santa Fe Trail and lawnmower problems. However K-Landscapes number of generations increased. This could be because the high mutation rate is inducing instability in the population, thus it would take more generations to find a solution. Changing the selection parameters had very little effect on Santa Fe Trail and lawnmower, however the number if generations required to reach a solution for K-Landscapes increased. Mutation and crossover changes induced large amounts of random variation into problems yet Santa Fe Trail and lawnmower showed little change in performance. I think this shows these problems are somewhat resistant to dramatic variation in its population. I think the opposite is true for K-Landscapes as parameters that induced variations increased the number of generations required to reach a final solution.

\end{document}